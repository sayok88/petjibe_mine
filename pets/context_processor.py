from .models import Petjibeuser, ContentCategory
from django.core.cache import cache


def user_profile_pic(request):
    if request.user.is_authenticated:
        petjibe_user = Petjibeuser.objects.filter(user__username=request.user.username)
        if petjibe_user:
            return {'pic':petjibe_user.first().get_profile_pic}
    return {'pic':'/media/profile_images/default.jpg'}


def categories(request):
    if "cats" not in cache:
        cache.set("cats", [{'name': c.content_cat.upper(), 'id': c.id} for c in ContentCategory.objects.all()],
                  1000000)
    return {"cats": cache.get("cats")}

