from urllib.parse import urljoin

from bs4 import BeautifulSoup
from django.conf import settings

from .models import ContentDetails
import requests
from celery import task
from PIL import Image
from requests import get
from io import BytesIO

def send_html_message(user, message,subject, html_msg=None):
    try:
        user.email_user(subject=subject, message=message, from_email=settings.DEFAULT_FROM_EMAIL,html_message=html_msg,
                        fail_silently=False)
    except Exception as e:
        print(str(e))


@task()
def get_article(id):
    pagetitle = ""
    art_img = ""
    art_desc = ""
    meta_desc = ""
    meta_title = ""
    og_img = ""
    og_title = ""
    og_desc = ""
    share_img = ""
    one_obj = ContentDetails.objects.get(id = int(id))
    res = requests.get(one_obj.cont_link)
    soup = None
    if res.status_code == 200:
        soup = BeautifulSoup(res.content, "html.parser")
        for data in soup.find_all("meta"):
            if data.get("name") == "DESCRIPTION" or data.get("name") == "description":
                meta_desc = data.get("content")
            elif data.get("http-equiv") == "TITLE" or data.get("http-equiv") == "title":
                meta_title = data.get("content")
            elif data.get("property") == "og:image":
                if og_img is not None or len(og_img) > 0:
                    og_img_link = data.get("content")
                    path_elem = og_img.split(".")
                    if "jpg" not in path_elem:
                        og_img = ""
                    else:
                        og_img = og_img_link
                else:
                    og_img_link = data.get("content")
                    path_elem = og_img.split(".")
                    if "jpg" not in path_elem:
                        og_img = ""
                    else:
                        og_img = og_img_link

            elif data.get("property") == "og:title":
                og_title = data.get("content")
            elif data.get("property") == "og:description":
                og_desc = data.get("content")
            elif data.get("name") == "shareaholic:image":
                share_img = data.get("content")
            else:
                pass

        page_title = soup.find("title").get_text()

        # =========== saving data to the database
        one_obj.cont_desc = meta_desc + "\n" + og_desc
        if len(meta_title) > 0 and meta_title is not None:
            one_obj.cont_heading = meta_title
        else:
            one_obj.cont_heading = page_title
        if len(og_img) > 0 and og_img is not None:
            one_obj.cont_image = og_img
            one_obj.img_flag = True
        elif share_img is not None and len(share_img) > 0:
            one_obj.cont_image = share_img
            one_obj.img_flag = True
        else:
            one_obj.cont_image = ""
        best_im = ""
        best_width = 100
        try:
            if not share_img and not og_img:
                for pic in soup.find_all('img'):
                    width = pic.get('width', 0)
                    if str(width) != "0":
                        width = str(width).replace("px","")
                        width = str(width).replace("%", "")

                    # else:
                    #     best_im = pic.get('src')
                    #     width = get_image_width(best_im,one_obj.cont_link)
                    print(width)
                    if str(width).isnumeric():
                        width = int(width)
                        if width> best_width:
                            best_width = width
                            best_im = pic.get('src')
            if best_width>100:
                if not best_im.startswith('http'):
                    best_im = urljoin(one_obj.cont_link, best_im)
                one_obj.cont_image = best_im
                one_obj.img_flag = True
        except Exception as e:
            print(e)
        one_obj.save()

def get_image_width(src,url):
    try:
        if not src.startswith('http'):
            src = urljoin(src, url)
        image_raw = get(src)
        image = Image.open(BytesIO(image_raw.content))
        width, height = image.size
    except Exception as e:
        print(e)
        width = "jali"
    return width