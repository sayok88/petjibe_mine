from django.apps import AppConfig
from django.db.models.signals import post_migrate

def create_default_pet_types(sender, **kwargs):
    if 'plan' in kwargs:
        if len(kwargs['plan'])>0:
            from .models import Pet_Type
            pet_types = Pet_Type.objects.all()
            if pet_types.count() == 0:
                Pet_Type(pet_type="Dog").save()
                Pet_Type(pet_type="Cat").save()


class PetsConfig(AppConfig):
    name = 'pets'

    def ready(self):
        post_migrate.connect(create_default_pet_types,sender=self)
