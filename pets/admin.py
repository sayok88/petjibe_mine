from django.contrib import admin
from django.apps import apps
from .models import City, ContentDetails

class CityAdmin(admin.ModelAdmin):
    model = City
    list_display = ['city_name', 'get_state', 'get_country']
    def get_country(self, obj):
        return obj.state.country.country_name
    def get_state(self, obj):
        return obj.state.state_name

    get_state.admin_order_field = 'state__state_name'  # Allows column order sorting
    get_state.short_description = 'State Name'  # Renames column head
    get_country.admin_order_field = 'state__country__country_name'  # Allows column order sorting
    get_country.short_description = 'Country Name'  # Renames column head
models = apps.get_app_config('pets').get_models()
class ContentAdmin(admin.ModelAdmin):
    change_list_template = 'admin_content.html'
for model in models:
    # print(model.__name__)
    if model.__name__ == 'City':
        admin.site.register(model, CityAdmin)
        continue
    if model.__name__ == 'ContentDetails':
        admin.site.register(model, ContentAdmin)
        continue

    admin.site.register(model)


