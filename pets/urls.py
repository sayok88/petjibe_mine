from django.shortcuts import render
from django.urls import path
from .views import get_profile_details, GetCities


def index(request):
    return render(request, 'v2/index.html', {})


urlpatterns = [
    path('v2/index/', index),
    path('getCities/', GetCities.as_view()),

    path('get_profile_details_ajax', get_profile_details)
]
