# README #

This README would normally document whatever steps are necessary to get your application up and running.






### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

Adding threaded comments

This package has build-in support for django-threadedcomments in this module. It can be enabled using the following settings:

INSTALLED_APPS += (
    'threadedcomments',
)

COMMENTS_APP = 'fluent_comments'

And make sure the intermediate ThreadedComment model is available and filled with data:

./manage.py migrate
./manage.py migrate_comments

django-celery-email - A Celery-backed Django Email Backend

A Django email backend that uses a Celery queue for out-of-band sending of the messages.
The templates and admin interface adapt themselves automatically to show the threaded comments.
To enable django-celery-email for your project you need to add djcelery_email to INSTALLED_APPS:

INSTALLED_APPS += ("djcelery_email",)

You must then set django-celery-email as your EMAIL_BACKEND:

EMAIL_BACKEND = 'djcelery_email.backends.CeleryEmailBackend'
