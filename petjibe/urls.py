"""petjibe URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() functisearchMsgon: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, include
from django.views.generic.base import TemplateView
from pets.views import *
from pets.urls import urlpatterns as urpattern2

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('get_content_types/', get_content_types, name='get_content_types'),
                  path('admin_upload_content_details/', admin_upload_content_details,
                       name='admin_upload_content_details'),
                  # url(r'^accounts/', include('allauth.urls')), # added for allauth
                  # ============ petjibe user login and registration
                  # path('join/',TemplateView.as_view(template_name="login.html"),name="join"),
                  path('signin/', TemplateView.as_view(template_name="v2/sign-in.html"), name="signin"),
                  path('signup/', TemplateView.as_view(template_name="v2/sign-up.html"), name="signup"),
                  path('registration/', SignupView.as_view()),
                  path('login/', LoginView.as_view()),
                  path('logout/', logout, name="logout"),
                  path('email_confirmation/<str:key>/', email_confirmation, name="email_confirmation"),
                  path('fb_token/', FbConnectView.as_view(), name="fb_token"),
                  # ============ pet registration
                  path('petRegisterPage/', petRegisterPage, name="petRegisterPage"),
                  path('pet-register/', PetRegisterView.as_view()),
                  path('pet-breed/', GetPetBreed.as_view()),
                  # =========== owner profile registration
                  path('ownerRegistration/', ownerRegistration, name="ownerRegistration"),
                  path('getStates/', GetStates.as_view()),
                  path('ownerRegistrationSubmit/', OwnerRegistrationSubmit.as_view()),
                  # =========== owner profile edit
                  path('ownerProfileEdit/', ownerProfileEdit, name="ownerProfileEdit"),
                  # =========== owner profile without pet
                  path('profileWithoutPetView/', profileWithoutPetView, name="profileWithoutPetView"),
                  # =========== owner profile with pet
                  path('ownerProfile/', profileWithPetView, name="ownerProfile"),
                  # =========== pet delete
                  path('petDelete/', PetDelete.as_view(), name="petDelete"),
                  # =========== pet edit
                  path('petEditView/<str:pet_id>', petEditView, name="petEditView"),
                  # =========== email subscription
                  path('email_subscription/', Email_subscriptions.as_view(), name="email_subscription"),
                  path('email_subscribe_valid/<str:key>/', email_subscription_valid, name="email_subscribe_valid"),
                  # =========== pet article
                  # path('petArticle/',TemplateView.as_view(template_name="pet-articles.html"),name="index"),
                  path('petArticle/<str:type>', article_index, name="petArticle"),
                  path('petArticleSingle/<str:art_id>', articleRedirectView, name="petArticleSingle"),
                  path('article/<int:a_id>', article_get, name="article_get"),
                  path('get_articles/<int:cat_id>/<int:page_no>/<int:no_articles>/', get_articles_by_page,
                       name='get_articles_by_page'),

                  # =========== pet forget password and change password
                  path('forgotpassword/', forgot_password, name="forgotpassword"),
                  path('resetpassword/<str:key>/', reset_password, name="resetpassword"),
                  path('changepassword/', change_password, name="changepassword"),
                  # =========== find extended family api call / view search results
                  path('searchresults/', searchResultApiView, name="searchresults"),
                  path('searchresultsview/', searchresultsview, name="searchresultsview"),
                  path('searchMsg/<str:uid>/', searchmsg, name="searchMsg"),
                  # =========== contact us
                  path('contactus/', ContactusView.as_view(), name="contactus"),
                  # =========== getting zipcode
                  path('getzip/', GetZipcode.as_view(), name="getzip"),
                  # ============ dashboard
                  # path('index/',TemplateView.as_view(template_name="index.html"),name="index"),
                  # path('index/',indexView,name="index"),
                  path('', indexView, name="index"),
                  # ============== messaging app
                  url(r"^messages/", include("pinax.messages.urls", namespace="pinax_messages")),
                  path('msgpage/', UserMsgView.as_view(), name="msgpage"),
                  path('newmsg/', newMessageView, name="newmsg"),
                  path('createmsgthread/', UserCreateMsgView.as_view(), name="createmsgthread"),
                  path('get_user_threads/', get_user_threads, name="get_user_threads"),
                  path('get_thread_message/', get_thread_message, name="get_thread_message"),
                  path('send_message/', send_message, name="send_message"),
                  path('createmultimsgthread/', UserCreateMultiMsgView.as_view(), name="createmultimsgthread"),
                  path('replymsgthread/', UserReplyMsgView.as_view(), name="replymsgthread"),
                  path('allmsgs/', allMessageView, name="allmsgs"),
                  path('chatbox/', TemplateView.as_view(template_name="chat-box.html"), name="chatbox"),
                  path('set_multi_email/', set_multi_emails, name="set_multi_email"),
                  path('upload_profile_pic/', user_upload_image, name="upload_profile_pic"),
                    path('upload_profile_pic_pet/', pet_upload_image, name="pet_upload_image"),
                  url(r'^blog/comments/', include('fluent_comments.urls')),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + urpattern2
