import pymysql
pymysql.install_as_MySQLdb()
from .celex import app as celery_app

__all__ = ['celery_app']